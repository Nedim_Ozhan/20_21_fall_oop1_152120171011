#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {

    int n, q;

    cin >> n >> q;

    vector<vector<int>> vec(n);
    vector<int> buffer;
    for (int i = 0; i < n; i++) {
        int k;
        cin >> k;
        for (int j = 0; j < k; j++) {
            int value;
            cin >> value;
            buffer.push_back(value);
        }
        vec[i] = buffer;
        buffer.clear();
    }

    for (int i = 0; i < q; i++) {
        int row, col;
        cin >> row >> col;
        cout << vec[row][col] << endl;
    }
    return 0;
}