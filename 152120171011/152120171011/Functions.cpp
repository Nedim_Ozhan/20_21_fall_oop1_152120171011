#include <iostream>
#include <cstdio>
using namespace std;


int max_of_four(int a, int b, int c, int d) {
    int max = a;
    int max_2 = c;
    if (max < b) {
        max = b;
    }
    if (max_2 < d) {
        max_2 = d;
    }
    if (max > max_2) {
        return max;
    }
    else {
        return max_2;
    }
}


int main() {
    int a, b, c, d;
    scanf("%d %d %d %d", &a, &b, &c, &d);
    int ans = max_of_four(a, b, c, d);
    printf("%d", ans);

    return 0;
}