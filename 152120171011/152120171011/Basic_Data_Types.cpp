#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    // Complete the code. int, long, char, float, and double,
    int int_;
    long long_;
    char char_;
    float float_;
    double double_;

    scanf("%d %ld %c %f %lf", &int_, &long_, &char_, &float_, &double_);
    printf("%d \n", int_);
    printf("%ld \n", long_);
    printf("%c \n", char_);
    printf("%f \n", float_);
    printf("%lf \n", double_);
    return 0;
}