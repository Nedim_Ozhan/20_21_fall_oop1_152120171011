#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
    vector<int> parsed;
    stringstream ss(str);

    int value;
    char comma;

    while (ss) {
        if (ss.peek() != ',') {
            if (ss >> value) {
                parsed.push_back(value);
            }
        }
        else {
            ss >> comma;
        }
    }

    return parsed;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}